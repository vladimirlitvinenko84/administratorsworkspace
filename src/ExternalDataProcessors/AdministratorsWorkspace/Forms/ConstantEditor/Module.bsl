
&AtServer
Procedure OnCreateAtServer(Cancel, StandardProcessing)
		
	If Not ThisForm.Parameters.Property("ConstantName", ConstantName) Then
		Cancel = True;
		StandardProcessing = False;
		Return;
	EndIf;
	
	ThisForm.Title = ConstantName;
	ThisForm.Parameters.Property("UseRu", UseRu);
	
	If UseRu Then
		Items.FormWriteAndClose.Title = "Записать и закрыть";
	EndIF;
		
		
	AttributesArray = new Array;	                                                                                                      //SavedData
	AttributesArray.Add(New FormAttribute(ConstantName, Metadata.Constants[ConstantName].Type, , Metadata.Constants[ConstantName].Synonym, True));	
	ChangeAttributes(AttributesArray);		
	ThisForm[ConstantName] = Constants[ConstantName].Get();
	
	NewItem = Items.Add(ConstantName, Type("FormField")); 
	NewItem.Type = FormFieldType.InputField;
	NewItem.DataPath = ConstantName;
	
EndProcedure


&AtClient
Procedure WriteAndClose(Command)
	
	Write(Command);
	ThisForm.Close();
	
EndProcedure


&AtClient
Procedure Write(Command)
	
	WriteAtServer();
		
EndProcedure


&AtServer
Procedure WriteAtServer()
	
	Constants[ConstantName].Set(ThisForm[ConstantName]);
	ThisForm.Modified = False;
		
EndProcedure